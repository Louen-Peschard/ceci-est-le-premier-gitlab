package formation.year2122.epsi.java.api.dto;

public class PowerDTO {
    private long id;
    private String powerName;
    private String description;

    public PowerDTO() {
    }

    public PowerDTO(long id, String powerName, String description) {
        this.id = id;
        this.powerName = powerName;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPowerName() {
        return powerName;
    }

    public void setPowerName(String powerName) {
        this.powerName = powerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String secretIdentity) {
        this.description = description;
    }
}
