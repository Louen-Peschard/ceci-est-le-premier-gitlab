package formation.year2122.epsi.java.api;

import formation.year2122.epsi.java.api.dto.PowerDTO;
import formation.year2122.epsi.java.model.Power;
import formation.year2122.epsi.java.repository.PowerRepository;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import net.bytebuddy.implementation.bind.annotation.Super;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(
        path = "/Powers",
        produces = {APPLICATION_JSON_VALUE}
)
public class PowerController {

    private final PowerRepository PowerRepository;

    PowerController(
            PowerRepository PowerRepository
    ) {
        this.PowerRepository = PowerRepository;
    }
    @GetMapping(path = "{id}")
    public ResponseEntity<PowerDTO> getById(@PathVariable Long id){
//        Optional<Power> optionalPower = this.PowerRepository.findById(id);
//        if(optionalPower.isPresent()){
//            Power Power = optionalPower.get();
//            PowerDTO PowerDTO = mapToDTO(Power);
//            return ResponseEntity.ok(PowerDTO);
//        }else{
//            return ResponseEntity.notFound().build();
//        }

        return this.PowerRepository.findById(id)
                .map(Power -> ResponseEntity.ok(mapToDTO(Power)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<List<PowerDTO>> getAll() {
//         List<Power> Powers = this.PowerRepository.findAll();
//         List<PowerDTO> PowerDTOS = new ArrayList<>();
//         Powers.forEach(Power -> PowerDTOS.add(mapToDTO(Power)));
//
//        return ResponseEntity.ok(PowerDTOS);
        return ResponseEntity.ok(
                this.PowerRepository
                        .findAll()
                        .stream()
                        .map(this::mapToDTO)
                        .collect(Collectors.toList())
        );
    }

    @GetMapping(path = "search")
    public ResponseEntity<List<PowerDTO>> searchByName(@RequestParam(name = "name") String name){
        return ResponseEntity.ok(
                this.PowerRepository
                        .findAll()
                        .stream()
                        .map(this::mapToDTO)
                        .collect(Collectors.toList())
        );
    }




    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<PowerDTO> create(@RequestBody PowerDTO PowerDTO){
        PowerDTO.setId(0);
        Power Power = mapToEntity(PowerDTO);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapToDTO(this.PowerRepository.save(Power)));

    }

    @PutMapping(path = "{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<PowerDTO> update(
            @PathVariable Long id,
            @RequestBody PowerDTO PowerDTO
    ) {
        if (this.PowerRepository.findById(id).isEmpty()) {
            return ResponseEntity.notFound()
                    //.body(new ErrorDTO("Message à changer", HttpStatus.NOT_FOUND))
                    .build();
        }
        if (id != PowerDTO.getId()) {
            return ResponseEntity.badRequest()
                    //.body(new ErrorDTO("Les deux id, dans le body et dans le path, ne correspondent pas", HttpStatus.BAD_REQUEST))
                    .build();
        }

        Power PowerToUpdate = mapToEntity(PowerDTO);
        return ResponseEntity.ok(mapToDTO(this.PowerRepository.save(PowerToUpdate)));
    }

    private Power mapToEntity(PowerDTO PowerDTO){
        Power Power = new Power();
        Power.setId(Power.getId());
        Power.setName(Power.getName());
        Power.setDescription(Power.getDescription());
        return Power;
    }

    private PowerDTO mapToDTO(Power Power) {
        return new PowerDTO(
                Power.getId(),
                Power.getName(),
                Power.getDescription()
        );
    }


}
